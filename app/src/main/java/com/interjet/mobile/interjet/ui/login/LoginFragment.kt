package com.interjet.mobile.interjet.ui.login

import android.content.DialogInterface
import kotlinx.android.synthetic.main.fragment_login.*
import com.interjet.mobile.interjet.InterjetApplication
import com.interjet.mobile.interjet.R
import com.interjet.mobile.interjet.base.BaseFragment
import com.interjet.mobile.interjet.ui.login.model.LoginResponse
import com.interjet.mobile.interjet.ui.login.model.User
import com.interjet.mobile.interjet.ui.presenters.LoginViewModel
import com.interjet.mobile.interjet.ui.views.LoginView
import com.interjet.mobile.interjet.utils.createDialog
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginView {

    @Inject
    lateinit var loginPresenter: LoginViewModel

    private lateinit var loginResponse: LoginResponse

    override fun getLayout(): Int =
            R.layout.fragment_login

    override fun initView() {
        super.initView()
        setupFragment()
        setListener()
    }

    private fun setListener() {
        this.login_btn.setOnClickListener {
            loginPresenter.login("mchaveza@ia.com.mx", "sky12.12")
        }
    }

    private fun setupFragment() {
        initializeDagger()
        loginPresenter.setViewReference(this)
    }

    override fun initializeDagger() {
        InterjetApplication.getApplicationComponent().inject(this@LoginFragment)
    }

    override fun onLoginSucceeded(response: LoginResponse) {
        loginResponse = response
        (activity as LoginActivity).addLocalAccount(User("dummy@example.com",
                "dummyPassword",
                response.accessToken,
                response.expireIn.toString(),
                response.tokenType))
    }

    override fun onLoginFailed(throwable: Throwable) {
        createDialog(
                context = context!!,
                message = throwable.message.toString(),
                okBtn = getString(R.string.accept),
                positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                }
        )
    }

    fun login() {
        (activity as LoginActivity).addLocalAccount(User("dummy@example.com",
                "dummyPassword",
                loginResponse.accessToken,
                loginResponse.expireIn.toString(),
                loginResponse.tokenType))
    }

    override fun showLoading() {
        loadingDialog.show(fragmentManager, LoginActivity::class.java.name)
    }

    override fun hideLoading() {
        loadingDialog.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        loginPresenter.stop()
    }

}