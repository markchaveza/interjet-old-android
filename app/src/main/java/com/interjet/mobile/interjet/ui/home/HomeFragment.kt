package com.interjet.mobile.interjet.ui.home

import com.interjet.mobile.interjet.InterjetApplication
import com.interjet.mobile.interjet.R
import com.interjet.mobile.interjet.base.BaseFragment
import com.interjet.mobile.interjet.data.ArgumentConstants
import com.interjet.mobile.interjet.ui.login.model.User

class HomeFragment : BaseFragment() {

    private var user: User? = null

    override fun getLayout(): Int =
            R.layout.fragment_home

    override fun initView() {
        super.initView()
        initializeDagger()
        recoverUser()
    }

    private fun recoverUser() {
        val bundle = arguments
        this.user = bundle?.getSerializable(ArgumentConstants.USER) as? User
    }

    override fun initializeDagger() {
        InterjetApplication.getApplicationComponent().inject(this)
    }

}