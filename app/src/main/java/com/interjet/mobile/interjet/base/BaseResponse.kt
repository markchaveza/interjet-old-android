package com.interjet.mobile.interjet.base

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class BaseResponse(

        @field:SerializedName("Mensaje")
        private val _message: String? = null,

        @field:SerializedName("Codigo")
        private val _code: Int? = null,

        @field:SerializedName("Estatus")
        private val _status: Int? = null

) : Serializable {

    val message: String
        get() = this._message ?: ""

    val code: Int
        get() = this._code ?: 0

    val status: Int
        get() = this._status ?: 0

}