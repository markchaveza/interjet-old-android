package com.interjet.mobile.interjet.data

class ArgumentConstants {

    companion object {
        const val USER = "user"

        const val NO_INTERNET = "No fue posible realizar la petición. Revisa tu conexión"
    }

}