package com.interjet.mobile.interjet.ui.presenters

import com.interjet.mobile.interjet.base.BaseViewModel
import com.interjet.mobile.interjet.data.DataConfiguration
import com.interjet.mobile.interjet.domain.LoginInteractor
import com.interjet.mobile.interjet.ui.login.model.LoginRequest
import com.interjet.mobile.interjet.ui.login.model.LoginResponse
import com.interjet.mobile.interjet.ui.views.LoginView
import javax.inject.Inject

class LoginViewModel
@Inject
constructor(private val loginInteractor: LoginInteractor) : BaseViewModel<LoginView>(), LoginInteractor.LoginCallback {

    init {
        setInteractorListener()
    }

    /**
     * With this we invoke the api call
     * to log into our account
     * @param user to be logged in
     * @param password that belongs to the user
     */
    fun login(user: String, password: String) {
        getView()?.showLoading()
        loginInteractor.login(buildLoginRequest(user, password))
    }

    /**
     * When login succeeded
     * @param loginResponse
     */
    override fun onSuccessLogin(loginResponse: LoginResponse) {
        getView()?.let {
            it.hideLoading()
            it.onLoginSucceeded(loginResponse)
        }
    }

    /**
     * When something went wrong with login
     * @param error
     */
    override fun onFailedLogin(error: Throwable) {
        getView()?.let {
            it.hideLoading()
            it.onLoginFailed(error)
        }
    }

    /**
     * Let's build LoginRequest object to invoke service
     * @param user
     * @param password
     */
    private fun buildLoginRequest(user: String, password: String): LoginRequest =
            LoginRequest(
                    DataConfiguration.CALL_ID,
                    DataConfiguration.CALL_ID_KEY,
                    user,
                    password
            )

    override fun onCleared() {
        super.onCleared()
        loginInteractor.stop()
    }

    private fun setInteractorListener() {
        this.loginInteractor.setListener(this)
    }

    @Suppress("DEPRECATION")
    fun setViewReference(view: LoginView) {
        setView(view)
        setInteractorListener()
    }

}



