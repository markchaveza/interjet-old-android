package com.interjet.mobile.interjet.data.repository

import com.interjet.mobile.interjet.ui.login.model.LoginRequest
import com.interjet.mobile.interjet.ui.login.model.LoginResponse
import rx.Observable

interface LoginRepository {

    fun login(loginRequest: LoginRequest): Observable<LoginResponse>

}