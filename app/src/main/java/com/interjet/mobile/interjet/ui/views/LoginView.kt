package com.interjet.mobile.interjet.ui.views

import com.interjet.mobile.interjet.base.BaseView
import com.interjet.mobile.interjet.ui.login.model.LoginResponse

interface LoginView : BaseView {
    fun onLoginSucceeded(response: LoginResponse)
    fun onLoginFailed(throwable: Throwable)
}