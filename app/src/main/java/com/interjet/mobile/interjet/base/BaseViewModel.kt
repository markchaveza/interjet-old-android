package com.interjet.mobile.interjet.base

import android.arch.lifecycle.ViewModel


open class BaseViewModel<T : BaseView> : ViewModel() {
    private var view: T? = null

    @Deprecated("Call setViewReference to avoid memory leaks.")
    fun setView(view: T?) {
        this.view = view
    }

    fun showLoading() {
        view?.showLoading()
    }

    fun hideLoading() {
        view?.hideLoading()
    }

    fun getView(): T? = this.view

}