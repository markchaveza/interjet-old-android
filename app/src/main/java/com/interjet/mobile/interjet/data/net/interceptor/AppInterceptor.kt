package com.interjet.mobile.interjet.data.net.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AppInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain?.request()
        return chain?.proceed(request)!!
    }

}