package com.interjet.mobile.interjet.data.net.entity

import com.interjet.mobile.interjet.data.net.services.LoginService
import com.interjet.mobile.interjet.data.net.utils.ResponseUtils
import com.interjet.mobile.interjet.data.repository.LoginRepository
import com.interjet.mobile.interjet.ui.login.model.LoginRequest
import com.interjet.mobile.interjet.ui.login.model.LoginResponse
import rx.Observable

class NetLoginEntity(private val loginService: LoginService) : LoginRepository {

    /**
     * We call LOGIN service to authenticate user
     * and we handle success and failure responses
     */
    override fun login(loginRequest: LoginRequest): Observable<LoginResponse> =
            loginService.login(loginRequest.callId, loginRequest.callIdKey, loginRequest.user, loginRequest.password)
                    .flatMap {
                        if (it.isSuccessful) {
                            if (it.body().status == 0) {
                                Observable.just(it.body())
                            } else {
                                Observable.error(ResponseUtils.processWrongBody(it))
                            }
                        } else {
                            Observable.error(ResponseUtils.processErrorResponse(it))
                        }
                    }

}