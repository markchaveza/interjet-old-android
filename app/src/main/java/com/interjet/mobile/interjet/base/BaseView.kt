package com.interjet.mobile.interjet.base

interface BaseView{
    fun showLoading()
    fun hideLoading()
}