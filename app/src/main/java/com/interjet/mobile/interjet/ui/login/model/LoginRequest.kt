package com.interjet.mobile.interjet.ui.login.model

import com.google.gson.annotations.SerializedName

data class LoginRequest(
        @SerializedName("CallId")
        val callId: String,

        @SerializedName("CallIdKey")
        val callIdKey: String,

        @SerializedName("usuario")
        val user: String,

        @SerializedName("pass")
        val password: String
)