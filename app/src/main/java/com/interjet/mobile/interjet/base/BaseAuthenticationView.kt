package com.interjet.mobile.interjet.base

interface BaseAuthenticationView : BaseView {
    fun onTokenExpired(message: String)
}