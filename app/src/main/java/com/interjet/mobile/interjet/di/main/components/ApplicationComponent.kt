package com.interjet.mobile.interjet.di.main.components

import com.interjet.mobile.interjet.ui.login.LoginFragment
import dagger.Component
import com.interjet.mobile.interjet.base.BaseActivity
import com.interjet.mobile.interjet.base.BaseActivityNoAuthentication
import com.interjet.mobile.interjet.base.BaseFragment
import com.interjet.mobile.interjet.di.data.DataModule
import com.interjet.mobile.interjet.di.domain.DomainModule
import com.interjet.mobile.interjet.di.main.modules.ApplicationModule
import com.interjet.mobile.interjet.ui.home.HomeFragment
import com.interjet.mobile.interjet.ui.splash.SplashActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (DataModule::class), (DomainModule::class)])
interface ApplicationComponent {
    fun inject(splashActivity: SplashActivity)
    fun inject(baseActivity: BaseActivity)
    fun inject(baseFragment: BaseFragment)
    fun inject(baseActivityNoAuthentication: BaseActivityNoAuthentication)
    fun inject(loginFragment: LoginFragment)
    fun inject(homeFragment: HomeFragment)
}