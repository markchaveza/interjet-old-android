package com.interjet.mobile.interjet

import android.support.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.interjet.mobile.interjet.di.data.DataModule
import com.interjet.mobile.interjet.di.main.components.ApplicationComponent
import com.interjet.mobile.interjet.di.main.components.DaggerApplicationComponent
import com.interjet.mobile.interjet.di.main.modules.ApplicationModule

class InterjetApplication : MultiDexApplication() {

    companion object {
        private lateinit var instance: InterjetApplication

        private lateinit var applicationComponent: ApplicationComponent

        private fun initDependencies(application: InterjetApplication) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(ApplicationModule(application))
                    .dataModule(DataModule(application.getString(R.string.base_url)))
                    .build()
        }

        fun getApplicationComponent() = applicationComponent

        fun getInstance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        initDependencies(this)
        Stetho.initializeWithDefaults(this)
        instance = this@InterjetApplication
    }

}