package com.interjet.mobile.interjet.base

import android.Manifest
import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.OnAccountsUpdateListener
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import com.ia.mchaveza.kotlin_library.PermissionManager
import com.interjet.mobile.interjet.R
import com.interjet.mobile.interjet.account.AccountsManager
import com.interjet.mobile.interjet.account.Authenticator.Companion.REQUEST_CODE_FOR_REMOVE_ACCOUNT
import com.interjet.mobile.interjet.account.Authenticator.Companion.REQUEST_PERMISSIONS_CODE
import com.interjet.mobile.interjet.account.PermissionsManager
import com.interjet.mobile.interjet.ui.login.model.User
import com.interjet.mobile.interjet.utils.createDialog

abstract class BaseAuthenticationActivity : AppCompatActivity(), OnAccountsUpdateListener {

    private var user: User? = null
    private val permissionManager by lazy { PermissionManager(this, null) }

    abstract fun onGetUser(user: User)

    private var isAccountListenerSet: Boolean = false

    override fun onStart() {
        super.onStart()
        if (!isAccountListenerSet) {
            addAccountUpdateListener(this, REQUEST_PERMISSIONS_CODE)
        }
    }

    override fun onStop() {
        super.onStop()
        if (isAccountListenerSet) {
            AccountManager.get(this).removeOnAccountsUpdatedListener(this)
            this.isAccountListenerSet = false
        }
    }

    override fun onAccountsUpdated(accounts: Array<out Account>?) {
        if (AccountsManager.accountExistByType(packageName, accounts ?: emptyArray())) {
            if (this.user == null) {
                val account: Account = AccountsManager.getAccountByType(this.packageName, accounts
                        ?: emptyArray())!!
                val accountManager: AccountManager = AccountManager.get(this)

                val email: String = accountManager.getUserData(account, "email") ?: ""
                val password: String = accountManager.getUserData(account, "password") ?: ""
                val accessToken: String = accountManager.getUserData(account, "accessToken") ?: ""
                val expireIn: String = accountManager.getUserData(account, "expireIn") ?: ""
                val tokenType: String = accountManager.getUserData(account, "tokenType") ?: ""

                this.user = User(email, password, accessToken, expireIn, tokenType)

                this.onGetUser(this.user!!)
            }
        } else {
            AccountManager.get(this).addAccount(packageName, null, null, null, this, null, null)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSIONS_CODE -> {
                if (PermissionsManager.verifyPermissions(grantResults)) {
                    if (this.isAccountListenerSet) {
                        AccountManager.get(this).removeOnAccountsUpdatedListener(this)
                    }
                    addAccountUpdateListener(this, REQUEST_PERMISSIONS_CODE)
                } else {
                    createDialog(
                            context = this@BaseAuthenticationActivity,
                            message = getString(R.string.permissions_manually),
                            okBtn = getString(R.string.accept),
                            isCancelable = false,
                            positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                                dialog.dismiss()
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", packageName, null))
                                startActivity(intent)
                                finish()
                            }
                    ).show()
                }
            }
            REQUEST_CODE_FOR_REMOVE_ACCOUNT -> if (PermissionsManager.verifyPermissions(grantResults)) {
                removeLocalAccount()
            }
        }
    }

    private fun addAccountUpdateListener(activity: Activity, requestCode: Int) {
        if (permissionManager.permissionGranted(Manifest.permission.GET_ACCOUNTS)) {
            AccountManager.get(this).addOnAccountsUpdatedListener(this, null, true)
            this.isAccountListenerSet = true
        } else {
            requestAccountPermissions(activity, requestCode)
        }
    }

    private fun requestAccountPermissions(activity: Activity, requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.GET_ACCOUNTS)) {
            createDialog(
                    context = this@BaseAuthenticationActivity,
                    message = getString(R.string.permissions_accounts),
                    okBtn = getString(R.string.accept),
                    isCancelable = false,
                    positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.GET_ACCOUNTS), requestCode)
                    },
                    negativeListener = DialogInterface.OnClickListener { _, _ ->
                        finish()
                    }
            ).show()
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.GET_ACCOUNTS), requestCode)
        }
    }

    @Suppress("DEPRECATION")
    fun removeLocalAccount() {
        if (permissionManager.permissionGranted(Manifest.permission.GET_ACCOUNTS)) {
            val accountManager = AccountManager.get(this@BaseAuthenticationActivity)
            if (AccountsManager.accountExistByType(packageName, accountManager.getAccountsByType(packageName))) {
                val account = accountManager.getAccountsByType(packageName)[0]
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    accountManager.removeAccount(account, this@BaseAuthenticationActivity, null, null)
                } else {
                    accountManager.removeAccount(account, null, null)
                }
            }
        } else {
            requestAccountPermissions(this, REQUEST_CODE_FOR_REMOVE_ACCOUNT)
        }
    }

}