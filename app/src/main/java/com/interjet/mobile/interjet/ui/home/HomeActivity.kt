package com.interjet.mobile.interjet.ui.home

import android.support.v4.app.Fragment
import com.interjet.mobile.interjet.R
import com.interjet.mobile.interjet.base.BaseActivity

class HomeActivity : BaseActivity() {

    private var homeFragment: HomeFragment? = null
    private var token: String? = null

    override fun getFragment(): Fragment {
        homeFragment = HomeFragment()
        return homeFragment!!
    }

    override fun initView() {
        super.initView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.rigth_in, R.anim.rigth_out)
    }

}