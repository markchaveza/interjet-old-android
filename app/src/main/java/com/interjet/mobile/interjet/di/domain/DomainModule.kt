package com.interjet.mobile.interjet.di.domain

import dagger.Module
import dagger.Provides
import com.interjet.mobile.interjet.data.repository.LoginRepository
import com.interjet.mobile.interjet.domain.LoginInteractor
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    fun provideLoginInteractor(repository: LoginRepository) =
            LoginInteractor(repository)

}