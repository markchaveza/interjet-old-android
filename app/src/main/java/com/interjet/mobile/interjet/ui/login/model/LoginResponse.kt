package com.interjet.mobile.interjet.ui.login.model

import com.google.gson.annotations.SerializedName
import com.interjet.mobile.interjet.base.BaseResponse

data class LoginResponse(

        @field:SerializedName("access_token")
        private val _accessToken: String? = null,

        @field:SerializedName("refresh_token")
        private val _refreshToken: String? = null,

        @field:SerializedName("token_type")
        private val _tokenType: String? = null,

        @field:SerializedName("expire_in")
        private val _expireIn: Int? = null
) : BaseResponse() {

    val accessToken: String
        get() = this._accessToken ?: ""

    val refreshToken: String
        get() = this._refreshToken ?: ""

    val tokenType: String
        get() = this._tokenType ?: ""

    val expireIn: Int
        get() = this._expireIn ?: 0

}