package com.interjet.mobile.interjet.di.data

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import com.interjet.mobile.interjet.data.DataConfiguration
import com.interjet.mobile.interjet.data.net.entity.NetLoginEntity
import com.interjet.mobile.interjet.data.net.interceptor.AppInterceptor
import com.interjet.mobile.interjet.data.net.services.LoginService
import com.interjet.mobile.interjet.data.repository.LoginRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataModule(private val baseUrl: String) {

    private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val httpClient: OkHttpClient.Builder
    private val gson: Gson

    init {
        logging.level = HttpLoggingInterceptor.Level.BODY

        httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.writeTimeout(15, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor(AppInterceptor())

        gson = GsonBuilder().setLenient().create()
    }

    @Provides
    @Singleton
    fun providesDataConfiguration(): DataConfiguration = DataConfiguration(baseUrl)

    @Provides
    @Singleton
    fun providesRetrofit(dataConfiguration: DataConfiguration): Retrofit =
            Retrofit.Builder()
                    .baseUrl(dataConfiguration.getBaseUrl())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build()

    @Provides
    @Singleton
    fun providesLoginRepository(retrofit: Retrofit): LoginRepository =
            NetLoginEntity(retrofit.create(LoginService::class.java))

}