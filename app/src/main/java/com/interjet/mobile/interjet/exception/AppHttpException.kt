package com.interjet.mobile.interjet.exception

class AppHttpException : AppException {

    var orsanResponseCode: Int = 0

    constructor(detailMessage: String) : super(detailMessage) {
        orsanResponseCode = -1
    }

    constructor(detailMessage: String, errorCode: Int) : super(detailMessage) {
        this.orsanResponseCode = errorCode
    }

    constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable) {
        orsanResponseCode = -1
    }

    constructor(detailMessage: String, throwable: Throwable, errorCode: Int) : super(detailMessage, throwable) {
        this.orsanResponseCode = errorCode
    }

    fun getResponseCode(): Int = this.orsanResponseCode

}



