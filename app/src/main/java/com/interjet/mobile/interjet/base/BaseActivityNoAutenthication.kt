package com.interjet.mobile.interjet.base

import android.Manifest
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.ia.mchaveza.kotlin_library.PermissionCallback
import com.ia.mchaveza.kotlin_library.PermissionManager
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import kotlinx.android.synthetic.main.activity_base.*
import com.interjet.mobile.interjet.InterjetApplication
import com.interjet.mobile.interjet.R
import com.interjet.mobile.interjet.account.AccountsManager
import com.interjet.mobile.interjet.ui.dialogs.LoadingDialog
import com.interjet.mobile.interjet.ui.home.HomeActivity
import com.interjet.mobile.interjet.ui.login.model.User
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

abstract class BaseActivityNoAuthentication : AppCompatActivity(), PermissionCallback {

    @Inject
    lateinit var preferences: SharedPreferencesManager

    private var mResultBundle: Bundle? = null
    lateinit var loadingDialog: LoadingDialog
    private var mAccountAuthenticatorResponse: AccountAuthenticatorResponse? = null
    private val permissionManager by lazy { PermissionManager(this, this) }

    abstract fun getFragment(): Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.left_in, R.anim.left_out)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.base_container, getFragment())
                .commitAllowingStateLoss()

        initView()
    }

    open fun initView() {
        initializeDagger()
        prepareSupportActionBar()
        loadingDialog = LoadingDialog()
        loadingDialog.isCancelable = false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.rigth_in, R.anim.rigth_out)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else -> false
        }
    }

    private fun initializeDagger() {
        InterjetApplication.getApplicationComponent().inject(this)
    }

    private fun prepareSupportActionBar() {
        this.base_toolbar.title = ""
        setSupportActionBar(this.base_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.base_toolbar.navigationIcon = ContextCompat.getDrawable(this@BaseActivityNoAuthentication, R.drawable.ic_back)
        this.base_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun addLocalAccount(user: User) {
        if (permissionManager.permissionGranted(Manifest.permission.GET_ACCOUNTS)) {
            val accountManager = AccountManager.get(this)
            val accounts = accountManager.getAccountsByType(packageName)

            if (accounts.isNotEmpty() && AccountsManager.accountExistByName(user.email, accounts)) {
                val oldAccount = AccountsManager.getAccountByName(user.email, accounts)
                accountManager.setPassword(oldAccount, user.password)

                setAccountAuthenticatorResult(user.bundle)

                val intent = Intent(this@BaseActivityNoAuthentication, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                val account = Account(user.email, packageName)
                val userData = user.bundle
                if (accountManager.addAccountExplicitly(account, user.password, userData)) {
                    setAccountAuthenticatorResult(userData)
                    val intent = Intent(this@BaseActivityNoAuthentication, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                } else {
                    longSnackbar(this.base_parent, R.string.error_adding_account_messsage)
                }
            }
        } else {
            permissionManager.requestSinglePermission(Manifest.permission.GET_ACCOUNTS)
        }
    }

    private fun setAccountAuthenticatorResult(result: Bundle) {
        this.mResultBundle = result
    }

    override fun finish() {
        if (this.mAccountAuthenticatorResponse != null) {
            if (this.mResultBundle != null) {
                this.mAccountAuthenticatorResponse?.onResult(this.mResultBundle)
            } else {
                this.mAccountAuthenticatorResponse?.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled")
            }
            this.mAccountAuthenticatorResponse = null
        }
        super.finish()
    }

    override fun onPermissionDenied(permission: String) {
    }

    override fun onPermissionGranted(permission: String) {
    }

}